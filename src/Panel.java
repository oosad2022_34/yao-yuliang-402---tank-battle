import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class Panel extends JPanel implements KeyListener {
    public Missile m = null;
    public Tank tank = new Tank(50, 50, 4);
    @Override
    public void paint(Graphics g) {
        super.paint(g);
        if (m != null){
            m.draw(g);
        }
        repaint();
    }
    /**/



    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_J){
            m = tank.fire();
        }
        repaint();
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }
}
