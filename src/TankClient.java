import java.awt.*;
import java.awt.Color;
import java.awt.Image;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Component;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.WindowListener;
import java.util.List;
import java.util.ArrayList;
import java.awt.event.KeyListener;
public class TankClient extends Frame{
	int x = 50;
	int y = 50;
	Image offScreenImage = null;
	public static final int GAME_WIDTH = 800;
	public static final int GAME_HEIGHT = 600;
	List<Tank> tanks = new ArrayList<Tank>();
	Blood b = new Blood();
	List<Explode> explodes = new ArrayList<Explode>();
	Tank myTank = new Tank(350, 450, true,Tank.Direction.STOP, this);
	List missiles = new ArrayList<Missile>();
	Wall w1 = new Wall(100, 300, 20, 100, this), w2 = new Wall(300, 250, 150, 20, this);
	Missile m = new Missile(50, 50, Tank.Direction.R);
	public void paint(Graphics g) {
		g.drawString("missiles count: " + missiles.size(), 10, 50);
		g.drawString("explodes count: " + explodes.size(), 10, 70);
		g.drawString("tanks count: " + tanks.size(), 10, 90);
		g.drawString("life:"+myTank.getLife(), 750, 50);
		for(int i = 0; i < missiles.size(); i++) { 
			Missile m =  (Missile) missiles.get(i); 
			m.hitTanks(tanks);
			m.collidesWithTank(myTank);
			m.hitWall(w1); 
			m.hitWall(w2);
			m.draw(g);
		}
		for(int i1 = 0; i1 < explodes.size(); i1++) { 
			Explode e = explodes.get(i1); 
			e.draw(g);
		}
		for (int i = 0; i < tanks.size(); i++) {
			Tank t = tanks.get(i);
			t.collidesWithWall(w1); 
			t.collidesWithWall(w2);
			t.collidesWithTanks(tanks);
			t.draw(g);
		}
		w1.draw(g);
		w2.draw(g);
		myTank.draw(g);
		b.draw(g);
		myTank.eat(b);
		if(tanks.size()<=0) {
			String over="Game over";
			String win="You win!";
			String a="Press the space to restart the game";
				g.setColor(Color.red);
	            g.setFont(new Font("΢ź",1,70));
	            g.drawString(over,240,260);
	            g.setFont(new Font("΢ź",1,50));
	            g.drawString(win,280,330);
	            g.setFont(new Font("΢ź",1,30));
	            g.drawString(a,160,570);
		}else {
			if(!myTank.isLive()) {
				String over = "Game over";
				String win = "You lose";
				String a = "Press F2 to revive";
	            g.setColor(Color.red);
	            g.setFont(new Font("΢ź",1,70));
	            g.drawString(over,240,260);
	            g.setFont(new Font("΢ź",1,50));
	            g.drawString(win,280,330);
	            g.setFont(new Font("΢ź",1,30));
	            g.drawString(a,260,570);
			}
		}
	}
	public void update(Graphics g) {
		if(offScreenImage == null) {
			offScreenImage = this.createImage(800, 600);        
		}
		Graphics gOffScreen = offScreenImage.getGraphics();
		Color c = gOffScreen.getColor();
		gOffScreen.setColor(Color.GREEN);
		gOffScreen.fillRect(0, 0, GAME_WIDTH, GAME_HEIGHT);
		gOffScreen.setColor(c);
		print(gOffScreen);
		g.drawImage(offScreenImage,0,0,null);
	}
	public void launchFrame() {
		for(int i = 0; i < 10; i++) {
			tanks.add(new Tank(50 + 40 * (i + 1), 50, false,Tank.Direction.D, this));
			}
		this.setLocation(300,100);
		this.setSize(GAME_WIDTH,GAME_HEIGHT);
		this.setTitle("TankWar");
		this.addWindowListener((WindowListener) new WindowAdapter() { public void windowClosing(WindowEvent e) {
				System.exit(0);
			}});
		
		this.setResizable(false);
		this.setBackground(Color.GREEN);
	    addKeyListener(new KeyMonitor());
		setVisible(true);
		new Thread(new PaintThread()).start();	
	}
	public class PaintThread implements Runnable{
		public void run() {
			while(true) {
				repaint();
				try {
					Thread.sleep(80);
					}
				catch(InterruptedException e) {
					e.printStackTrace();
					}
				}
		}
		
	}
	private class KeyMonitor extends KeyAdapter{
		public void keyPressed(KeyEvent e) {
			myTank.KeyPressed(e);			
		}
		public void keyReleased(KeyEvent e){
				myTank.keyReleased(e);
		}
	}
	public static void main(String[] args) {
		TankClient tc= new TankClient();
		tc.launchFrame();
	}
}