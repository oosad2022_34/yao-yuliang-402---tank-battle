import javax.swing.*;
import java.awt.*;

public class TanClient extends JFrame {
	public Panel view = new Panel();

	public void launchFrame(){
		this.setTitle("坦克大战");
		this.setSize(1300,800);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
		this.add(view);
		this.addKeyListener(view);
	}

	public static void main(String[] args) {
		TanClient tc = new TanClient();
		tc.launchFrame();
		/**/
	}

}
